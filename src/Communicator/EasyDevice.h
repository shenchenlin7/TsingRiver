#ifndef __EASY_DEVICE__
#define __EASY_DEVICE__

#include <chrono>
#include <thread>

#include <poll.h>
#include <fcntl.h>
#include <arpa/inet.h>
#include <netinet/in.h>
#include <sys/socket.h>
#include <unistd.h>

#include <map>
#include <vector>
#include <string.h>
#include <json/json.h>

#include "../CommonLand/Farmland.h"

namespace Communicator
{
    #define MAX_BUF_SIZE        4096
    #define MAX_CLIENT_SIZE     1000
    #define MAX_SOCK_BUF_SIZE   32768

    struct EdPoolData
    {
    #define ED_PD_ST_SUCCESS		0
    #define ED_PD_ST_FINISHED		1
    #define ED_PD_ST_ERROR			2
    #define ED_PD_ST_EVENT			3
        int         state;
        int         fd;
        std::string data;
    };

    class EasyDeviceS
    {
    private:
        int                 m_sockFd{0};
        int                 m_listenPort{0};
        CommLand::Farmland  *m_poolQueue{nullptr};
    public:
        EasyDeviceS()=default;
        virtual ~EasyDeviceS()=default;
        EasyDeviceS(const EasyDeviceS& easyDevice)=delete;
        EasyDeviceS& operator=(const EasyDeviceS& easyDevice)=delete;
    public:
        int getPort()   { return m_listenPort; }
    public:
        int init(int MinPort, int MaxPort);
        void deinit();
        bool getEdPoolData(struct EdPoolData& poolData);

    private:
        static void *routine(void *arg);

        int createSocket(int port);
        int sendData(int fd, const Json::Value &data);
    };

    class EasyDeviceC
    {
    private:
        int                 m_sockFd{0};
        int                 m_serverPort{0};
        CommLand::Farmland  *m_poolQueue{nullptr};
    public:
        EasyDeviceC()=default;
        virtual ~EasyDeviceC()=default;
        EasyDeviceC(const EasyDeviceC& easyDevice)=delete;
        EasyDeviceC& operator=(const EasyDeviceC& easyDevice)=delete;
    public:
        int init(int serverPort);
        void deinit();
        bool getEdPoolData(struct EdPoolData& poolData);
        int sendData(const Json::Value &data);
    public:
        int getSocketFd()   { return m_sockFd; }
    private:
        static void *routine(void *arg);

        int createSocket(int port);
    };
}

#endif //__EASY_DEVICE__