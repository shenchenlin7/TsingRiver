#include "EasyDevice.h"

namespace Communicator
{
    int EasyDeviceS::init(int MinPort, int MaxPort)
    {
        for (int port = MinPort; port < MaxPort; port++)
        {
            this->m_sockFd = createSocket(port);
            if (this->m_sockFd > 0)
            {
                this->m_listenPort = port;
                pthread_t tid;
                if (0 != pthread_create(&tid, NULL, routine, this))
                    close(this->m_sockFd);

                break;
            }

            std::this_thread::sleep_for(std::chrono::milliseconds(100));
        }

        if (this->m_poolQueue == nullptr)
            this->m_poolQueue = CommLand::FarmlandWork::create(4096, sizeof(struct EdPoolData));

        return 1;
    }

    void EasyDeviceS::deinit()
    {
        close(this->m_sockFd);

        if (this->m_poolQueue != nullptr)
            CommLand::FarmlandWork::destroy(this->m_poolQueue);
    }

    bool EasyDeviceS::getEdPoolData(struct EdPoolData& poolData)
    {
        struct EdPoolData *getPoolData = (struct EdPoolData *)CommLand::FarmlandWork::get(this->m_poolQueue);
        if (!getPoolData)
            return false;

        poolData.state = getPoolData->state;
        poolData.fd = getPoolData->fd;
        poolData.data = getPoolData->data;

        delete getPoolData;

        return true;
    }

    int EasyDeviceS::createSocket(int port)
    {
        struct sockaddr_in servaddr;
        int listenfd = -1;

        if (-1 == (listenfd = socket(PF_INET, SOCK_STREAM, 0)))
        {
            close(listenfd);
            return -1;
        }

        int reuseaddr = 1;
        if (-1 == setsockopt(listenfd, SOL_SOCKET, SO_REUSEADDR, &reuseaddr, sizeof(reuseaddr)))
        {
            close(listenfd);
            return -1;
        }

        int iRet = 0;
        int iSockbufLen = 0;
        socklen_t iLen = sizeof(iSockbufLen);

        iRet = getsockopt(listenfd, SOL_SOCKET, SO_SNDBUF, (void *)&iSockbufLen, &iLen);
        if (iRet < 0) 
        {
            close(listenfd);
            return -1;
        }

        if (iSockbufLen < MAX_SOCK_BUF_SIZE)
        {
            iSockbufLen = MAX_SOCK_BUF_SIZE;
            iRet = setsockopt(listenfd, SOL_SOCKET, SO_SNDBUF, (void *)&iSockbufLen, iLen);
            if (iRet < 0) 
            {
                close(listenfd);
                return -1;
            }

            iRet = getsockopt(listenfd, SOL_SOCKET, SO_SNDBUF, (void *)&iSockbufLen, &iLen);
            if (iRet < 0) 
            {
                close(listenfd);
                return -1;
            }
        }

        memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = PF_INET;
        servaddr.sin_port = htons(port);
        servaddr.sin_addr.s_addr = INADDR_ANY;

        if (-1 == bind(listenfd, (struct sockaddr*)&servaddr, sizeof(servaddr)))
        {
            close(listenfd);
            return -1;
        }

        if (-1 == listen(listenfd, MAX_CLIENT_SIZE))
        {
            close(listenfd);
            return -1;
        }

        return listenfd;
    }

    void *EasyDeviceS::routine(void *arg)
    {
        EasyDeviceS *device = (EasyDeviceS *)arg;
        int listenfd = device->m_sockFd;

        struct pollfd fds[MAX_CLIENT_SIZE];

        struct pollfd fdsSock;
        fdsSock.fd = listenfd;
        fdsSock.events = POLLIN | POLLERR;
        fdsSock.revents = 0;
        int conncount = 0;

        std::map<int, struct pollfd> pollList;
        pollList.clear();
        pollList.insert(std::make_pair(listenfd, fdsSock));
        std::vector<int> toDel;
        std::map<int, struct pollfd> toAdd;

        while(1)
        {
            toDel.clear();
            toAdd.clear();

            conncount = 0;
            memset(fds, 0, sizeof(struct pollfd) * MAX_CLIENT_SIZE);

            //将pollList里面的数据同步到fds，poll调用需要的类型中
            std::map<int, struct pollfd>::iterator it = pollList.begin();
            for (; it != pollList.end(); it++)
            {
                struct pollfd fdIt = it->second;
                fds[conncount].fd = fdIt.fd;
                fds[conncount].events = fdIt.events;
                fds[conncount].revents = fdIt.revents;
                conncount++;
            }

            int ret = poll(fds, conncount, -1);
            if (ret <= 0)
            {
                std::this_thread::sleep_for(std::chrono::milliseconds(100));
                continue;
            }

            for (int i = 0; i < conncount; i++)
            {
                //收到fd挂起或者ERR信号，断开连接
                if ((fds[i].revents & POLLRDHUP) || (fds[i].revents & POLLERR))
                {
                    int fd = fds[i].fd;
                    close(fd);

                    struct EdPoolData *poolData = new EdPoolData();
                    poolData->state = ED_PD_ST_FINISHED;
                    poolData->fd = fd;
                    CommLand::FarmlandWork::put(poolData, device->m_poolQueue);

                    //将要删除的fd插入到vector中，后面一起处理
                    toDel.push_back(fd);
                    continue;
                }
                //新的连接请求上来
                else if ((fds[i].fd == listenfd) && (fds[i].revents & POLLIN))
                {
                    struct sockaddr_in client;
                    socklen_t lenaddr = sizeof(client);
                    int conn = -1;

                    if (-1 == (conn = accept(listenfd, (struct sockaddr*)&client, &lenaddr)))
                    {
                        continue;
                    }

                    fcntl(conn, F_SETFL, (fcntl(conn, F_GETFL) | O_NONBLOCK)); 

                    //创建struct pollfd结构，并将他插入到toAdd的map中，后面统一处理
                    struct pollfd tmpFd;
                    tmpFd.fd = conn;
                    tmpFd.events = POLLIN | POLLRDHUP | POLLERR;
                    tmpFd.revents = 0;
                    toAdd.insert(std::make_pair(conn, tmpFd));
                    continue;
                }
                //对应的fd有数据可读
                else if (fds[i].revents & POLLIN)
                {
                    char buf[MAX_BUF_SIZE] = {0,};
                    int lenrecv = recv(fds[i].fd, buf, MAX_BUF_SIZE - 1, 0);
                    if (lenrecv > 0)
                    {
                        struct EdPoolData *poolData = new EdPoolData();
                        poolData->state = ED_PD_ST_EVENT;
                        poolData->fd = fds[i].fd;
                        poolData->data = std::string(buf);
                        CommLand::FarmlandWork::put(poolData, device->m_poolQueue);
                    }

                    fds[i].revents &= (~POLLIN);
                }
            }

            //将pollList中，toDel的fd删除
            std::vector<int>::iterator itDel = toDel.begin();
            for (; itDel != toDel.end(); itDel++)
            {
                int fd = *itDel;
                std::map<int, struct pollfd>::iterator itList = pollList.find(fd);
                if (itList != pollList.end())
                    pollList.erase(itList);
            }

            //将toAdd的fd加入到pollList中
            std::map<int, struct pollfd>::iterator itAdd = toAdd.begin();
            for (; itAdd != toAdd.end(); itAdd++)
            {
                pollList.insert(std::make_pair(itAdd->first, itAdd->second));
            }

        }

        return NULL;
    }

    int EasyDeviceS::sendData(int fd, const Json::Value &data)
    {
        std::string startFlag = "#Pkg0#", endFlag = "#Pkg1#";
        std::string msg = data.toStyledString();
        std::string finalMsg = startFlag + msg + endFlag;

        return send(fd, finalMsg.c_str(), finalMsg.size(), 0);
    }

    int EasyDeviceC::init(int serverPort)
    {
        this->m_sockFd = createSocket(serverPort);
        if (this->m_sockFd <= 0)
            return -1;

        pthread_t tid;
        if (0 != pthread_create(&tid, NULL, routine, this))
        {
            close(this->m_sockFd);
            return -1;
        }

        if (this->m_poolQueue == nullptr)
            this->m_poolQueue = CommLand::FarmlandWork::create(4096, sizeof(struct EdPoolData));

        return 0;
    }

    void EasyDeviceC::deinit()
    {
        close(this->m_sockFd);
    }

    bool EasyDeviceC::getEdPoolData(struct EdPoolData& poolData)
    {
        struct EdPoolData *getPoolData = (struct EdPoolData *)CommLand::FarmlandWork::get(this->m_poolQueue);
        if (!getPoolData)
            return false;
            
        poolData.state = getPoolData->state;
        poolData.fd = getPoolData->fd;
        poolData.data = getPoolData->data;

        delete getPoolData;
        return true;
    }
    
    void *EasyDeviceC::routine(void *arg)
    {
        EasyDeviceC *device = (EasyDeviceC *)arg;
        int sockFd = device->m_sockFd;
        int recvLen = 0;
        std::string sockData = "";

        struct pollfd fds;
        fds.fd = sockFd;
        fds.events = POLLIN | POLLERR | POLLRDHUP;
        fds.revents = 0;

        while(true)
        {
            int pollRet = poll(&fds, 1, 100);
            if (pollRet < 0)
            {
                break;
            }

            if ((fds.revents & POLLRDHUP) || (fds.revents & POLLERR)) 
            {
                break;
            }
            else if (fds.revents & POLLIN) 
            {
                char recvValue[4096] = {0};
                if ((recvLen = recv(sockFd, recvValue, 4096, 0)) == -1)
                {
                    break;
                }
                else if (recvLen == 0)
                {
                    break;
                }
                else 
                {
                    sockData += recvValue;

                    Json::Reader reader;
                    Json::Value value = Json::nullValue;
                    std::string startFlag = "#Pkg0#", endFlag = "#Pkg1#";
                    size_t startFlagLen = startFlag.size(), endFlagLen = endFlag.size();
                    size_t start = sockData.find(startFlag);
                    size_t end = sockData.find(endFlag);
                    while (start != std::string::npos && end != std::string::npos)
                    {
                        size_t length = end - (start + startFlagLen);
                        std::string data = sockData.substr(start + startFlagLen, length);
                        if (reader.parse(data.c_str(), value, false))
                        {
                            struct EdPoolData *poolData = new EdPoolData();
                            poolData->state = ED_PD_ST_EVENT;
                            poolData->fd = sockFd;
                            poolData->data = data;
                            CommLand::FarmlandWork::put(poolData, device->m_poolQueue);
                        }

                        if (end + endFlagLen < sockData.size())
                            sockData = sockData.substr(end + endFlagLen);
                        else
                            sockData = "";

                        start = sockData.find(startFlag, start);
                        end = sockData.find(endFlag, end);
                    }
                }

                fds.events =  POLLIN | POLLERR | POLLRDHUP;
            }
        }
        
        return NULL;
    }

    int EasyDeviceC::createSocket(int port)
    {
        int sockFd = -1;
        char recvValue[4096] = {0};
        struct sockaddr_in servaddr;
        if ((sockFd = socket(PF_INET, SOCK_STREAM, 0)) < 0) 
        {
            return -1;
        }

        memset(&servaddr, 0, sizeof(servaddr));
        servaddr.sin_family = PF_INET;
        servaddr.sin_port = htons(port);
        servaddr.sin_addr.s_addr = INADDR_ANY;

        if (connect(sockFd, (struct sockaddr*)&servaddr, sizeof(servaddr)) < 0) 
        {
            close(sockFd);
            return -1;
        }

        return sockFd;
    }

    int EasyDeviceC::sendData(const Json::Value &data)
    {
        std::string msg = data.toStyledString();

        return send(m_sockFd, msg.c_str(), msg.size(), 0);
    }
}