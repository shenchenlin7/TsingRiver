#include "TrWater.h"

namespace TsingRiver
{
    void Drip::dripDone()
    {
        Drip *cur = this;
        Stream *parent;

        while (1)
        {
            parent = cur->m_parent;
            cur = cur->done();
            if (cur)
            {
                cur->m_parent = parent;
                cur->dispatch();
            }
            else if (parent)
            {
                if (__sync_sub_and_fetch(&parent->m_nleft, 1) == 0)
                {
                    cur = parent;
                    continue;
                }
            }
            break;
        }
    }

    void Stream::dispatch()
    {
        Drip **end = this->m_drips + this->m_dripNum;
        Drip **p = this->m_drips;

        this->m_nleft = this->m_dripNum;
        if (this->m_nleft != 0)
        {
            do
            {
                (*p)->m_parent = this;
                (*p)->dispatch();
            } while (++p != end);
        }
        else
            this->dripDone();
    }
}