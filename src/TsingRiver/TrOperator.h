#ifndef __TR_OPERATOR__
#define __TR_OPERATOR__

#include "TsingRiver.h"

namespace TsingRiver
{
    static inline Brook& operator>(Brook& x, Drip& y);
    static inline Brook& operator>(Brook& x, Drip *y);
    static inline Brook& operator>(Drip& x, Drip& y);
    static inline Brook& operator>(Drip& x, Drip *y);
    static inline Brook& operator>(Drip *x, Drip& y);
    static inline Brook& operator>(Brook& x, River& y);
    static inline Brook& operator>(River& x, River& y);
    static inline Brook& operator>(River& x, Drip& y);
    static inline Brook& operator>(River& x, Drip *y);
    static inline Brook& operator>(Drip& x, River& y);
    static inline Brook& operator>(Drip *x, River& y);

    static inline River& operator*(River& x, Brook& y);
    static inline River& operator*(Brook& x, River& y);
    static inline River& operator*(River& x, Drip& y);
    static inline River& operator*(River& x, Drip *y);
    static inline River& operator*(Drip& x, River& y);
    static inline River& operator*(Drip *x, River& y);
    static inline River& operator*(Brook& x, Brook& y);
    static inline River& operator*(Brook& x, Drip& y);
    static inline River& operator*(Brook& x, Drip *y);
    static inline River& operator*(Drip& x, Brook& y);
    static inline River& operator*(Drip *x, Brook& y);
    static inline River& operator*(Drip& x, Drip& y);
    static inline River& operator*(Drip& x, Drip *y);
    static inline River& operator*(Drip *x, Drip& y);

    //Brook
    static inline Brook& operator>(Brook& x, Drip& y)
    {
        x.pushBack(&y);
        return x;
    }
    
    static inline Brook& operator>(Brook& x, Drip *y)
    {
        return x > *y;
    }

    static inline Brook& operator>(Drip& x, Drip& y)
    {
        Brook *brook = TsingRiver::createBrook(&x, nullptr);
        brook->pushBack(&y);
        return *brook;
    }

    static inline Brook& operator>(Drip& x, Drip *y)
    {
        return x > *y;
    }

    static inline Brook& operator>(Drip *x, Drip& y)
    {
        return *x > y;
    }

    static inline Brook& operator>(Brook& x, River& y)
    {
        return x > (Drip&)y;
    }
    
    static inline Brook& operator>(River& x, Brook& y)
    {
        return y > (Drip&)x;
    }
    
    static inline Brook& operator>(River& x, River& y)
    {
        return (Drip&)x > (Drip&)y;
    }
    
    static inline Brook& operator>(River& x, Drip& y)
    {
        return (Drip&)x > y;
    }

    static inline Brook& operator>(River& x, Drip *y)
    {
        return x > *y;
    }
    
    static inline Brook& operator>(Drip& x, River& y)
    {
        return x > (Drip&)y;
    }

    static inline Brook& operator>(Drip *x, River& y)
    {
        return *x > y;
    }

    //River
    static inline River& operator*(River& x, Brook& y)
    {
        x.addBrook(&y);
        return x;
    }
    
    static inline River& operator*(Brook& x, River& y)
    {
        return y * x;
    }
    
    static inline River& operator*(River& x, Drip& y)
    {
        x.addBrook(TsingRiver::createBrook(&y, nullptr));
        return x;
    }

    static inline River& operator*(River& x, Drip *y)
    {
        return x * (*y);
    }
    
    static inline River& operator*(Drip& x, River& y)
    {
        return y * x;
    }

    static inline River& operator*(Drip *x, River& y)
    {
        return (*x) * y;
    }
    
    static inline River& operator*(Brook& x, Brook& y)
    {
        Brook *arr[2] = {&x, &y};
        return *TsingRiver::createRiver(arr, 2, nullptr);
    }
    
    static inline River& operator*(Brook& x, Drip& y)
    {
        return x * (*TsingRiver::createBrook(&y, nullptr));
    }

    static inline River& operator*(Brook& x, Drip *y)
    {
        return x * (*y);
    }
    
    static inline River& operator*(Drip& x, Brook& y)
    {
        return y * x;
    }

    static inline River& operator*(Drip *x, Brook& y)
    {
        return (*x) * y;
    }
    
    static inline River& operator*(Drip& x, Drip& y)
    {
        Brook *arr[2] = {TsingRiver::createBrook(&x, nullptr), TsingRiver::createBrook(&y, nullptr)};
        return *TsingRiver::createRiver(arr, 2, nullptr);
    }

    static inline River& operator*(Drip& x, Drip *y)
    {
        return x * (*y);
    }

    static inline River& operator*(Drip *x, Drip& y)
    {
        return (*x) * y;
    }
}

#endif //__TR_OPERATOR__