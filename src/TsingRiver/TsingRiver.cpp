#include "TsingRiver.h"

namespace TsingRiver
{
    Brook::Brook(Drip *first, Brook_callback&& cb): m_callback(std::move(cb))
    {
        this->m_queue = this->m_buf;
        this->m_queueSize = sizeof this->m_buf / sizeof *this->m_buf;
        first->setPointer(this);
        this->m_first = first;
    }

    Brook::~Brook()
    {
        if (this->m_queue != this->m_buf)
            delete []this->m_queue;
    }

    void Brook::expandQueue()
    {
        int size = 2 * this->m_queueSize;
        Drip **queue = new Drip *[size];
        int i = 0, j = this->m_front;

        do
        {
            queue[i++] = this->m_queue[j++];
            if (j == this->m_queueSize)
                j = 0;
        } while (j != this->m_back);

        if (this->m_queue != this->m_buf)
            delete []this->m_queue;

        this->m_queue = queue;
        this->m_queueSize = size;
        this->m_front = 0;
        this->m_back = i;
    }

    void Brook::pushBack(Drip *drip)
    {
        this->m_mutex.lock();

        drip->setPointer(this);
        this->m_queue[this->m_back] = drip;
        if (++this->m_back == this->m_queueSize)
            this->m_back = 0;

        if (this->m_front == this->m_back)
            this->expandQueue();

        this->m_mutex.unlock();
    }

    Drip *Brook::pop()
    {
        bool canceled = this->m_canceled;
        Drip *drip = this->popDrip();

        if (!canceled)
            return drip;

        while (drip)
        {
            delete drip;
            drip = this->popDrip();
        }

        return NULL;
    }

    Drip *Brook::popDrip()
    {
        Drip *drip;

        this->m_mutex.lock();

        if (this->m_front != this->m_back)
        {
            drip = this->m_queue[this->m_front];
            if (++this->m_front == this->m_queueSize)
                this->m_front = 0;
        }
        else
        {
            drip = this->m_last;
            this->m_last = NULL;
        }

        this->m_mutex.unlock();

        if (!drip)
        {
            this->m_finished = true;

            if (this->m_callback)
                this->m_callback(this);

            if (!this->m_inRiver)
                delete this;
        }

        return drip;
    }

    void Brook::dismissRecursive()
    {
        Drip *drip = this->m_first;

        this->m_callback = nullptr;
        do
        {
            delete drip;
            drip = this->popDrip();
        } while (drip);
    }

    River::River(River_callback&& cb): Stream(new Drip *[2 * 4], 0), m_callback(std::move(cb)), m_bufSize(4)
    {
        this->m_allBrook = (Brook **)&this->m_drips[4];
    }

    River::River(Brook *const allRiver[], size_t n, River_callback&& cb): Stream(new Drip *[2 * (n > 4 ? n : 4)], n), m_callback(std::move(cb))
    {
        this->m_bufSize = (n > 4 ? n : 4);
        this->m_allBrook = (Brook **)&this->m_drips[this->m_bufSize];
        for (size_t i = 0; i < n ; i++)
        {
            if (false == allRiver[i]->m_inRiver)
            {
                allRiver[i]->m_inRiver = true;
                this->m_allBrook[i] = allRiver[i];
                this->m_drips[i] = allRiver[i]->m_first;
            }
        }
    }

    River::~River()
    {
        for (size_t i = 0; i < this->m_dripNum ; i++)
        {
            this->m_allBrook[i]->m_inRiver = false;
            this->m_allBrook[i]->dismissRecursive();
        }

        delete []this->m_drips;
    }

    void River::expandBuf()
    {
        Drip **buf;
        size_t size;

        this->m_bufSize *= 2;
        buf = new Drip *[2 * this->m_bufSize];
        size = this->m_dripNum * sizeof (void *);
        memcpy(buf, this->m_drips, size);
        memcpy(buf + this->m_bufSize, this->m_allBrook, size);

        delete []this->m_drips;
        this->m_drips = buf;
        this->m_allBrook = (Brook **)&buf[this->m_bufSize];
    }

    void River::addBrook(Brook *brook)
    {
        if (this->m_dripNum == this->m_bufSize)
            this->expandBuf();

        if (false == brook->m_inRiver)
        {
            brook->m_inRiver = true;
            this->m_allBrook[this->m_dripNum] = brook;
            this->m_drips[this->m_dripNum] = brook->m_first;
            this->m_dripNum++;
        }
    }

    Drip *River::done()
    {
        Brook *brook = brookOf(this);

        if (this->m_callback)
            this->m_callback(this);

        for (size_t i = 0; i < this->m_dripNum ; i++)
            delete this->m_allBrook[i];

        this->m_dripNum = 0;
        delete this;
        
        return brook->pop();
    }
}