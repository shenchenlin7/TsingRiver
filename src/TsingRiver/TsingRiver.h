#ifndef __TSING_RIVER__
#define __TSING_RIVER__

#include <mutex>
#include <string.h>
#include "TrWater.h"

namespace TsingRiver
{
    class TsingRiver;
    class Brook;
    class River;

    using Brook_callback = std::function<void (const Brook *)>;
    using River_callback = std::function<void (const River *)>;

    class TsingRiver
    {
    public:
        static Brook *createBrook(Drip *first, Brook_callback cb);
        static void startBrookWork(Drip *first, Brook_callback cb);
        
        static River *createRiver(River_callback cb);
        static River *createRiver(Brook *const brooks[], size_t n, River_callback cb);
        static void startRiverWork(Brook *const brooks[], size_t n, River_callback cb);
    };

    static inline Brook *brookOf(const Drip *drip)
    {
        return (Brook *)drip->getPointer();
    }

    class Brook
    {
    private:
        Drip            *m_buf[4];
        Drip            *m_first;
        Drip            *m_last{NULL};
        Drip            **m_queue;
        int             m_queueSize;
        int             m_front{0};
        int             m_back{0};
        bool            m_inRiver{false};
        bool            m_canceled{false};
        bool            m_finished{false};
        std::mutex      m_mutex;

    protected:
        void            *m_context{NULL};
        Brook_callback  m_callback;

    protected:
        Brook(Drip *first, Brook_callback&& callback);
        virtual ~Brook();

    public:
        void start()                        { if (false == m_inRiver) { this->m_first->dispatch(); } }

        void setCallback(Brook_callback cb) { this->m_callback = std::move(cb); }
        void setContext(void *context)      { this->m_context = context; }
        void setCancel()                    { this->m_canceled = true; }
        void unsetLastDrip()                { this->m_last = NULL; }

        void *getContext() const            { return this->m_context; }
        bool isCanceled() const             { return this->m_canceled; }
        bool isFinished() const             { return this->m_finished; }

        void setLastDrip(Drip *last)        { last->setPointer(this); this->m_last = last; }
        
    protected:   
        void setInRiver()                   { this->m_inRiver = true; }

        Drip *getLastDrip() const           { return this->m_last; }

    public:
        void pushBack(Drip *drip);   
        Drip *pop();
    private:
        Drip *popDrip();
        void expandQueue();

    protected:
        void dismissRecursive();

        friend class River;
        friend class TsingRiver;
    };

    inline Brook *TsingRiver::createBrook(Drip *first, Brook_callback cb)
    {
        return new Brook(first, std::move(cb));
    }

    inline void TsingRiver::startBrookWork(Drip *first, Brook_callback cb)
    {
        new Brook(first, std::move(cb));
        first->dispatch();
    }

    class River: public Stream
    {
    private:
        size_t          m_bufSize;
        Brook           **m_allBrook;

    protected:
        void            *m_context{NULL};
        River_callback  m_callback;

    protected:
        River(River_callback&& callback);
        River(Brook *const brooks[], size_t n, River_callback&& callback);
        virtual ~River();

    public:
        void start() { if (!brookOf(this)); { TsingRiver::startBrookWork(this, nullptr); } }

    private:
        Brook *brookAt(size_t index)                { return (index < this->m_dripNum ? this->m_allBrook[index] : NULL); }
        const Brook *brookAt(size_t index) const    { return (index < this->m_dripNum ? this->m_allBrook[index] : NULL); }

    public:
        void setContext(void *context)              { this->m_context = context; }
        void setCallback(River_callback cb)         { this->m_callback = std::move(cb); }
        
        void *getContext() const                    { return this->m_context; }
        size_t size() const                         { return this->m_dripNum; }

        Brook& operator[] (size_t index)            { return *this->brookAt(index); }
        const Brook& operator[] (size_t index) const{ return *this->brookAt(index); }

    public:
        void addBrook(Brook *brook);

    private:
        void expandBuf();

    protected:
        virtual Drip *done();

        friend class TsingRiver;
    };

    inline River *TsingRiver::createRiver(River_callback cb)
    {
        return new River(std::move(cb));
    }

    inline River *TsingRiver::createRiver(Brook *const brooks[], size_t n, River_callback cb)
    {
        return new River(brooks, n, std::move(cb));
    }

    inline void TsingRiver::startRiverWork(Brook *const brooks[], size_t n, River_callback cb)
    {
        River *p = new River(brooks, n, std::move(cb));
        TsingRiver::startBrookWork(p, nullptr);
    }
}

#endif //__TSING_RIVER__