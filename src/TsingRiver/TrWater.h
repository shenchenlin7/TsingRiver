#ifndef __TR_WATER__
#define __TR_WATER__

#include <stddef.h>
#include <functional>

namespace TsingRiver
{
    class Drip;
    class Stream;

    class Drip
    {
    private:
        Stream      *m_parent{nullptr};
        void        *m_pointer{nullptr};

    public:
        Drip()=default;
        virtual ~Drip()=default;

    public:
        virtual void dispatch() = 0;

    private:
        virtual Drip *done() = 0;

    protected:
        void dripDone();

    public:
        void *getPointer() const        { return this->m_pointer; }
        void setPointer(void *pointer)  { this->m_pointer = pointer; }

        friend class Stream;
    };

    class Stream: public Drip
    {
    protected:
        Drip        **m_drips;
        size_t      m_dripNum;

    private:
        size_t      m_nleft{0};

    public:	
        Stream(Drip **drips, size_t n):m_drips(drips), m_dripNum(n) { }
        virtual ~Stream()=default;

    public:
        virtual void dispatch();

        friend class Drip;
    };
}

#endif //__TR_WATER__