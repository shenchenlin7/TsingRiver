#ifndef __TR_FACTORY__
#define __TR_FACTORY__

#include "TrTask/TrObImpl.h"
#include "TrTask/TrTimerImpl.h"
#include "CommonLand/CommonLand.h"

#include "TsingRiver/TrOperator.h"

namespace TrTask
{

    class TrFactory
    {
    public:
        static ObServerTask *createTaskOb(const std::function<int (ObServerTask *)> subscribe, std::function<void (ObServerTask *)> notify);
        static TimeGeneralTask *createTaskTimer(int expire, TimeController *controller, std::function<void (TimeGeneralTask *)> cb);
    
        static CommLand::Waterwheel *createWaterwheel();
    };
}

#endif //__TR_FACTORY__