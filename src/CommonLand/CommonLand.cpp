#include "CommonLand.h"

namespace CommLand
{
    class __DispatchManager
    {
    private:
        static bool     created_;

        Waterwheel      waterwheel_;
        FarmPool        *farmPool_; 

    private:
        __DispatchManager()
        {
            waterwheel_.init();
            farmPool_ = FarmPoolWork::create(Overman::getGlobalSettings()->workThreads, 0);
        }

        ~__DispatchManager()
        {
            waterwheel_.deInit();
            FarmPoolWork::destroy(this->farmPool_);
        }

    public:
        static __DispatchManager *getInstance()
        {
            static __DispatchManager kInstance;
            __DispatchManager::created_ = true;
            return &kInstance;
        }
        
    public:
        Waterwheel *getWaterwheel() { return &waterwheel_; }
        static bool isCreated()     { return created_; }
        FarmPool *getFarmPool()     { return farmPool_; }

    };

    bool __DispatchManager::created_ = false;

    class __CommunicatorManager
    {
    private:
        static bool     created_;

        EasyLens        easyLens_;

    private:
        __CommunicatorManager()
        {
            if (Overman::getGlobalSettings()->openEasyDeviceCS == 1)
                easyLens_.Communicator::EasyDeviceC::init(Overman::getGlobalSettings()->easyDevicePort);
            if (Overman::getGlobalSettings()->openEasyDeviceCS == 0)
                easyLens_.Communicator::EasyDeviceS::init(Overman::getGlobalSettings()->minEasyDevicePort, Overman::getGlobalSettings()->maxEasyDevicePort);
        }

        ~__CommunicatorManager()
        {
            easyLens_.Communicator::EasyDeviceC::deinit();
            easyLens_.Communicator::EasyDeviceS::deinit();
        }

    public:
        static __CommunicatorManager *getInstance()
        {
            static __CommunicatorManager kInstance;
            __CommunicatorManager::created_ = true;
            return &kInstance;
        }
        
    public:
        EasyLens *getEasyLens()     { return &easyLens_; }
        static bool isCreated()     { return created_; }

    };

    bool __CommunicatorManager::created_ = false;


    struct GlobalSettings Overman::settings_ = GLOBAL_SETTINGS_DEFAULT;

    bool Overman::isCreated()
    {
        return __DispatchManager::isCreated();
    }

    Waterwheel *Overman::getWaterwheel()
    {
        return __DispatchManager::getInstance()->getWaterwheel();
    }

    FarmPool *Overman::getFarmPool()
    {
        return __DispatchManager::getInstance()->getFarmPool();
    }

    EasyLens *Overman::getEasyLens()
    {
        return __CommunicatorManager::getInstance()->getEasyLens();
    }
}