#ifndef __COMMON_LAND__
#define __COMMON_LAND__

#include "../TimeWheel/TimeWheel.h"
#include "../Communicator/EasyDevice.h"
#include "FarmPool.h"

namespace Communicator
{
    class                               EasyDeviceS;
    class                               EasyDeviceC;
}

namespace CommLand
{
    class                               TimerSeed;
    class                               Waterwheel;

    class                               Tower;
    class                               TowerOperator;
    class                               EasyLens;

    class                               Farmer;    
    class                               Overman;

    struct GlobalSettings
    {
        int workThreads;
        int openEasyDeviceCS;   //-1:colse  0:server 1:client
        int easyDevicePort;
        int minEasyDevicePort;
        int maxEasyDevicePort;
    };

    static constexpr struct GlobalSettings GLOBAL_SETTINGS_DEFAULT =
    {
        .workThreads        =   4,
        .openEasyDeviceCS   =   -1,
        .easyDevicePort     =   0,
        .minEasyDevicePort  =   40000,
        .maxEasyDevicePort  =   50000,
    };


    /// @brief Timer
    class TimerSeed: public TimeWheel::TwSession
    {
    public:
        TimerSeed(int expire): TimeWheel::TwSession(expire) { }
        virtual ~TimerSeed()=default; 
    };

    class Waterwheel: public TimeWheel::TimeWheel
    {
    public:
        bool addTimerSeed(TimerSeed *seed)
        {
            return addSession(seed);
        }
    };

    /// @brief Communicator
    class Tower
    {
    public:
        virtual bool init() = 0;
        virtual void deinit() = 0;
    };

    class TowerOperator
    {
    };

    class EasyLens: virtual public Communicator::EasyDeviceS,
                    virtual public Communicator::EasyDeviceC
    {
    public:
        int getPort() { return Communicator::EasyDeviceS::getPort(); }
        bool getEdSPoolData(struct Communicator::EdPoolData& poolData)  { return Communicator::EasyDeviceS::getEdPoolData(poolData); }
        bool getEdCPoolData(struct Communicator::EdPoolData& poolData)  { return Communicator::EasyDeviceC::getEdPoolData(poolData); }

        int getEdCSocketFd()                                            { return Communicator::EasyDeviceC::getSocketFd(); }
        int sendEdCData(const Json::Value &data)                        { return Communicator::EasyDeviceC::sendData(data); }
    };

    /// @brief Golbal
    class Overman
    {
    public:
        static const struct GlobalSettings *getGlobalSettings()
        {
            return &settings_;
        }

        static void setGlobalSettings(const struct GlobalSettings *settings)
        {
            settings_ = *settings;
        }
    public:
        static bool isCreated();
        static class Waterwheel *getWaterwheel(); 
        static class FarmPool *getFarmPool();
        static class EasyLens *getEasyLens();
    private:
	    static struct GlobalSettings settings_;
    };
}

#endif //__COMMON_LAND__