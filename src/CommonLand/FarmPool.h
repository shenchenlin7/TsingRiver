#ifndef __FARM_POOL__
#define __FARM_POOL__

#include "Farmland.h"

namespace CommLand
{
    class   FarmPool;
    class   FarmPoolTask;
    class   FarmPoolTaskEntry;
    class   FarmPoolWork;

    class FarmPool
    {
    public:
        Farmland            *m_land;
        size_t              m_nthreads{0};
        size_t              m_stacksize;
        pthread_t           m_tid;
        pthread_mutex_t     m_mutex;
        pthread_key_t       m_key;
        pthread_cond_t      *m_terminate{NULL};
    public:
        FarmPool()=default;
        ~FarmPool()=default;
    };

    class FarmPoolTask
    {
    public:
        void                (*m_routine)(void *);
        void                *m_context;
    public:
        FarmPoolTask()=default;
        ~FarmPoolTask()=default;
    };

    class FarmPoolTaskEntry
    {
    public:
        void                *m_link;
        struct FarmPoolTask m_task;
    public:
        FarmPoolTaskEntry()=default;
        ~FarmPoolTaskEntry()=default;
    };

    class FarmPoolWork
    {
    private:
        static pthread_t m_zeroTid;
    public:
        static FarmPool *create(size_t nthreads, size_t stacksize);
        static int schedule(const struct FarmPoolTask *task, FarmPool *pool);
        static int increase(FarmPool *pool);
        static void destroy(FarmPool *pool);
    private:
        static int inPool(FarmPool *pool);
        static int createThreads(size_t nthreads, FarmPool *pool);
        static void *routine(void *arg);
        static void terminate(int in_pool, FarmPool *pool);
    };
}

#endif //__FARM_POOL__