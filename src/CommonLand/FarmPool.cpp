#include "FarmPool.h"
#include <string.h>

namespace CommLand
{
    pthread_t FarmPoolWork::m_zeroTid;

    FarmPool *FarmPoolWork::create(size_t nthreads, size_t stacksize)
    {
        FarmPool *pool;

        pool = (FarmPool *)malloc(sizeof (FarmPool));
        if (!pool)
            return NULL;

        pool->m_land = FarmlandWork::create((size_t)-1, 0);
        if (pool->m_land)
        {
            int ret = pthread_mutex_init(&pool->m_mutex, NULL);
            if (ret == 0)
            {
                ret = pthread_key_create(&pool->m_key, NULL);
                if (ret == 0)
                {
                    pool->m_stacksize = stacksize;
                    memset(&pool->m_tid, 0, sizeof (pthread_t));
                    if (createThreads(nthreads, pool) >= 0)
                        return pool;

                    pthread_key_delete(pool->m_key);
                }

                pthread_mutex_destroy(&pool->m_mutex);
            }

            FarmlandWork::destroy(pool->m_land);
        }

        free(pool);
        return NULL;
    }

    int FarmPoolWork::schedule(const FarmPoolTask *task, FarmPool *pool)
    {
        FarmPoolTaskEntry *buf;
        buf = (FarmPoolTaskEntry *)malloc(sizeof (FarmPoolTaskEntry));
        if (!buf)
            return -1;

        buf->m_task = *task;
	    FarmlandWork::put(buf, pool->m_land);
        return 0;
    }

    int FarmPoolWork::increase(FarmPool *pool)
    {
        pthread_attr_t attr;
        int ret = pthread_attr_init(&attr);
        if (ret == 0)
        {
            if (pool->m_stacksize)
                pthread_attr_setstacksize(&attr, pool->m_stacksize);

            pthread_mutex_lock(&pool->m_mutex);
            pthread_t tid;
            ret = pthread_create(&tid, &attr, routine, pool);
            if (ret == 0)
                pool->m_nthreads++;

            pthread_mutex_unlock(&pool->m_mutex);
            pthread_attr_destroy(&attr);
            if (ret == 0)
                return 0;
        }

        return -1;
    }

    void FarmPoolWork::destroy(FarmPool *pool)
    {
        int inpool = inPool(pool);
        terminate(inpool, pool);

        pthread_key_delete(pool->m_key);
        pthread_mutex_destroy(&pool->m_mutex);
        FarmlandWork::destroy(pool->m_land);
        if (!inpool)
            free(pool);
    }

    int FarmPoolWork::inPool(FarmPool *pool)
    {
        return pthread_getspecific(pool->m_key) == pool;
    }

    int FarmPoolWork::createThreads(size_t nthreads, FarmPool *pool)
    {
        pthread_attr_t attr;
        
        int ret = pthread_attr_init(&attr);
        if (ret == 0)
        {
            if (pool->m_stacksize)
                pthread_attr_setstacksize(&attr, pool->m_stacksize);

            while (pool->m_nthreads < nthreads)
            {
                pthread_t tid;
                ret = pthread_create(&tid, &attr, routine, pool);
                if (ret == 0)
                    pool->m_nthreads++;
                else
                    break;
            }

            pthread_attr_destroy(&attr);
            if (pool->m_nthreads == nthreads)
                return 0;

            terminate(0, pool);
        }

        return -1;
    }

    void *FarmPoolWork::routine(void *arg)
    {
        FarmPool *pool = (FarmPool *)arg;

        pthread_setspecific(pool->m_key, pool);
        while (!pool->m_terminate)
        {
            FarmPoolTaskEntry *entry;
            entry = (FarmPoolTaskEntry *)FarmlandWork::get(pool->m_land);
            if (!entry)
                break;

            void (*task_routine)(void *);
            task_routine = entry->m_task.m_routine;
            void *task_context = entry->m_task.m_context;
            free(entry);
            task_routine(task_context);

            if (pool->m_nthreads == 0)
            {
                free(pool);
                return NULL;
            }
        }

        pthread_mutex_lock(&pool->m_mutex);
        pthread_t tid = pool->m_tid;
        pool->m_tid = pthread_self();
        if (--pool->m_nthreads == 0)
            pthread_cond_signal(pool->m_terminate);

        pthread_mutex_unlock(&pool->m_mutex);
        if (memcmp(&tid, &m_zeroTid, sizeof (pthread_t)) != 0)
            pthread_join(tid, NULL);

        return NULL;
    }

    void FarmPoolWork::terminate(int inPool, FarmPool *pool)
    {
        pthread_cond_t term = PTHREAD_COND_INITIALIZER;

        pthread_mutex_lock(&pool->m_mutex);
        pool->m_terminate = &term;

        if (inPool)
        {
            pthread_detach(pthread_self());
            pool->m_nthreads--;
        }

        while (pool->m_nthreads > 0)
            pthread_cond_wait(&term, &pool->m_mutex);

        pthread_mutex_unlock(&pool->m_mutex);
        if (memcmp(&pool->m_tid, &m_zeroTid, sizeof (pthread_t)) != 0)
            pthread_join(pool->m_tid, NULL);
    }
}