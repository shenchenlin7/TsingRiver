#include "Farmland.h"

namespace CommLand
{
    Farmland *FarmlandWork::FarmlandWork::create(size_t maxlen, int m_linkoff)
    {
        Farmland *fland = (Farmland *)malloc(sizeof (Farmland));
        if (!fland)
            return NULL;

        int ret = pthread_mutex_init(&fland->m_getMutex, NULL);
        if (ret == 0)
        {
            ret = pthread_mutex_init(&fland->m_putMutex, NULL);
            if (ret == 0)
            {
                ret = pthread_cond_init(&fland->m_getCond, NULL);
                if (ret == 0)
                {
                    ret = pthread_cond_init(&fland->m_putCond, NULL);
                    if (ret == 0)
                    {
                        fland->m_msgMax = maxlen;
                        fland->m_linkoff = m_linkoff;
                        fland->m_getHead = &fland->m_head1;
                        fland->m_putHead = &fland->m_head2;
                        fland->m_putTail = &fland->m_head2;
                        return fland;
                    }
                    pthread_cond_destroy(&fland->m_getCond);
                }
                pthread_mutex_destroy(&fland->m_putMutex);
            }
            pthread_mutex_destroy(&fland->m_getMutex);
        }

        free(fland);

        return NULL;
    }

    void FarmlandWork::put(void *msg, Farmland *fland)
    {
        void **link = (void **)((char *)msg + fland->m_linkoff);
        *link = NULL;

        pthread_mutex_lock(&fland->m_putMutex);
        while (fland->m_msgCnt > (fland->m_msgMax - 1))
            pthread_cond_wait(&fland->m_putCond, &fland->m_putMutex);

        *fland->m_putTail = link;
        fland->m_putTail = link;
        fland->m_msgCnt++;
        pthread_mutex_unlock(&fland->m_putMutex);
        pthread_cond_signal(&fland->m_getCond);
    }

    void *FarmlandWork::get(Farmland *fland)
    {
        void *msg = NULL;

        pthread_mutex_lock(&fland->m_getMutex);
        if (*fland->m_getHead || swap(fland) > 0)
        {
            msg = (char *)*fland->m_getHead - fland->m_linkoff;
            *fland->m_getHead = *(void **)*fland->m_getHead;
        }
        pthread_mutex_unlock(&fland->m_getMutex);

        return msg;
    }

    void FarmlandWork::destroy(Farmland *fland)
    {
        pthread_cond_destroy(&fland->m_putCond);
        pthread_cond_destroy(&fland->m_getCond);
        pthread_mutex_destroy(&fland->m_putMutex);
        pthread_mutex_destroy(&fland->m_getMutex);
        
        free(fland);
    }

    size_t FarmlandWork::swap(Farmland *fland)
    {
        void **m_getHead = fland->m_getHead;

        fland->m_getHead = fland->m_putHead;
        pthread_mutex_lock(&fland->m_putMutex);
        while (fland->m_msgCnt == 0)
            pthread_cond_wait(&fland->m_getCond, &fland->m_putMutex);

        size_t cnt = fland->m_msgCnt;
        if (cnt > fland->m_msgMax - 1)
            pthread_cond_broadcast(&fland->m_putCond);

        fland->m_putHead = m_getHead;
        fland->m_putTail = m_getHead;
        fland->m_msgCnt = 0;
        pthread_mutex_unlock(&fland->m_putMutex);

        return cnt;
    }
}