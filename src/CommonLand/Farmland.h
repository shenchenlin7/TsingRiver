#ifndef __FARMLAND__
#define __FARMLAND__

#include <stddef.h>
#include <pthread.h>
#include <stdlib.h>

namespace CommLand
{
    class                              Farmland;
    class                              FarmlandWork;

    class Farmland
    {
    public:
        size_t          m_msgMax;
        size_t          m_msgCnt{0};
        int             m_linkoff;
        void            *m_head1{NULL};
        void            *m_head2{NULL};
        void            **m_getHead;
        void            **m_putHead;
        void            **m_putTail;
        pthread_mutex_t m_getMutex;
        pthread_mutex_t m_putMutex;
        pthread_cond_t  m_getCond;
        pthread_cond_t  m_putCond;
    public:
        Farmland()=default;
        ~Farmland()=default;
    };

    class FarmlandWork
    {
    public:
        static Farmland *create(size_t maxlen, int m_linkoff);
        static void put(void *msg, Farmland *fland);
        static void *get(Farmland *fland);
        static void destroy(Farmland *fland);
    private:
        static size_t swap(Farmland *fland);
    };
}

#endif //__FARMLAND__