#ifndef __TR_OB_IMPL__
#define __TR_OB_IMPL__

#include "TrObTask.h"

namespace TrTask
{
    class ObSubjectD;
    class ObNotifyD;
    class ObWork;

    using ObServerTask = ObTask<ObSubjectD, ObNotifyD>;

    class ObSubjectD
    {
    public:
        int                         m_subject;
        std::string                 m_account;
    public:
        ObSubjectD()=default;
        ~ObSubjectD()=default;
    };

    class ObNotifyD
    {
    public:
        int                         m_state;
        std::string                 m_reason;
    public:
        ObNotifyD()=default;
        ~ObNotifyD()=default;
    };

    class ObWork
    {
    public:
        static ObServerTask *createTaskOb(std::function<int (ObServerTask *)> subscribe, std::function<void (ObServerTask *)> notify, std::function<void (ObServerTask *)> cb);
    };
}

#endif //__TR_OB_IMPL__