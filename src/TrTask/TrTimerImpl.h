#ifndef __TR_TIMER_IMPL__
#define __TR_TIMER_IMPL__

#include "TrTimerTask.h"

namespace TrTask
{
    class TimerInputD;
    class TimerOutputD;
    class TimerWork;

    using TimeGeneralTask = TimerTask<TimerInputD, TimerOutputD>;

    class TimerInputD
    {
    public:
        std::string                 m_id;
        long long                   m_beginTime;
    public:
        TimerInputD()=default;
        ~TimerInputD()=default;
    };

    class TimerOutputD
    {
    public:
        TimerOutputD()=default;
        ~TimerOutputD()=default;
    };

    class TimerWork
    {
    public:
        static TimeGeneralTask *createTaskTimer(int expire, TimeController *controller, std::function<void (TimeGeneralTask *)> cb);
    };
}

#endif //__TR_TIMER_IMPL__