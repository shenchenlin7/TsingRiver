#ifndef __TR_OB_TASK__
#define __TR_OB_TASK__

#include "TrTaskCommon.h"

namespace TrTask
{
    class                               ObSubject;
    class                               ObServer;
    class                               ObRequest;

    class Observer
    {
    protected:
        Observer()=default;
        virtual ~Observer()=default;
    private:
        virtual void onEvent(int state) = 0;

        friend class ObSubject;
    };

    class ObSubject
    {
    protected:
        ObSubject()=default;
        virtual ~ObSubject()=default;
    protected:
        void onEvent(Observer* observer, int state)
        {
            if (observer != nullptr) 
                observer->onEvent(state);
        }
    };

    class ObRequest: public TrDrip,
                     public Observer
    {
    protected:
        int                             m_state{TR_STATE_INIT};
        
    public:
        ObRequest()=default;
        virtual ~ObRequest()=default;

    private:
        virtual int subscribe() = 0;
        virtual void notify() = 0;
    public:
        virtual void dispatch()         { this->handle(this->subscribe()); }

    protected:
        void handle(int state)
        {
            this->m_state = state;
            if (state < TR_STATE_INIT)
                this->dripDone();
        }

    private:
        virtual void onEvent(int state)
        {
            this->m_state = state;
            notify();
            this->dripDone();
        }
    };

    template<class INPUT, class OUTPUT>
    class ObTask: public ObRequest
    {
    protected:
        INPUT                                           m_input;
        OUTPUT                                          m_output;
        std::function<void (ObTask<INPUT, OUTPUT>*)>    m_callback;  
        std::function<int (ObTask<INPUT, OUTPUT>*)>     m_subscribe;  
        std::function<void (ObTask<INPUT, OUTPUT>*)>    m_notify;  
        
    public:
        ObTask(std::function<int (ObTask<INPUT, OUTPUT>*)> && subscribe, 
               std::function<void (ObTask<INPUT, OUTPUT>*)> && notify,
               std::function<void (ObTask<INPUT, OUTPUT>*)> && callback):
               m_subscribe(std::move(subscribe)), m_notify(std::move(notify)), m_callback(std::move(callback)) { }

	    virtual ~ObTask()=default;

    public:
        void start()                    { if (!brookOf(this)) { TsingRiver::TsingRiver::startBrookWork(this, nullptr); } }

    public:
        INPUT *getInput()               { return &this->m_input; }
        OUTPUT *getOutput()             { return &this->m_output; }

    private:
        virtual int subscribe()         { return m_subscribe(this); }
        virtual void notify()           { if(m_notify) { m_notify(this); } }

    protected:
        virtual TrDrip *done()
        {
            TrBrook *brook = brookOf(this);

            if (this->m_callback)
                this->m_callback(this);

            delete this;
            return brook->pop();
        }
    };
}

#endif //__TR_OB_TASK__