#include "TrObImpl.h"

namespace TrTask
{
    ObServerTask *ObWork::createTaskOb(std::function<int (ObServerTask *)> subscribe, std::function<void (ObServerTask *)> notify, std::function<void (ObServerTask *)> cb)
    {
        return new ObServerTask(std::move(subscribe), std::move(notify), std::move(cb)); 
    }
}