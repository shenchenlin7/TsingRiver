#ifndef __TR_TASK_COMMON__
#define __TR_TASK_COMMON__

#include "../TsingRiver/TsingRiver.h"
#include "../TsingRiver/TrOperator.h"

#include "../CommonLand/CommonLand.h"

#include <string.h>

namespace TrTask
{
    #define TR_STATE_CANCELED   -3
    #define TR_STATE_ERROR      -2
    #define TR_STATE_FINISHED   -1
    #define TR_STATE_INIT       0
    #define TR_STATE_LISTENING  1
    #define TR_STATE_RUNNING    1

    typedef TsingRiver::Drip            TrDrip;
    typedef TsingRiver::Brook           TrBrook;
}

#endif //__TR_TASK_COMMON__