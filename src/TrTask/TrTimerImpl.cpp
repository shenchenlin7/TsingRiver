#include "TrTimerImpl.h"

namespace TrTask
{
    TimeGeneralTask *TimerWork::createTaskTimer(int expire, TimeController *controller, std::function<void (TimeGeneralTask *)> cb)
    {
        return new TimeGeneralTask(expire, controller, std::move(cb)); 
    }
}