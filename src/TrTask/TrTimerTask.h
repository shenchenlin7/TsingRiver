#ifndef __TR_TIMER_TASK__
#define __TR_TIMER_TASK__

#include "TrTaskCommon.h"

namespace TrTask
{
    class                               TimerRequest;

    typedef CommLand::TimerSeed         Timer;
    typedef CommLand::Waterwheel        TimeController;


    class TimerRequest: public TrDrip,
                        public Timer
    {
    protected:
        int                              m_state{TR_STATE_INIT};
        TimeController                   *m_controller;
        
    public:
        TimerRequest(int expire, TimeController *controller): Timer(expire), m_controller(controller) {}
        virtual ~TimerRequest()=default;

    public:
        virtual void dispatch()         { this->handle(this->m_controller->addTimerSeed(this)); }

    protected:
        void handle(bool ret)
        {
            this->m_state = ret ? TR_STATE_RUNNING : TR_STATE_ERROR;
            if (this->m_state < TR_STATE_INIT)
                this->dripDone();
        }

    public:
        virtual void timeCallback()
        {
            this->m_state = TR_STATE_FINISHED;
            this->dripDone();
        }
    };

    template<class INPUT, class OUTPUT>
    class TimerTask: public TimerRequest
    {
    protected:
        INPUT                               m_input;
        OUTPUT                              m_output;
        std::function<void (TimerTask<INPUT, OUTPUT> *)>   m_callback;  
        void                                *m_userdata{NULL};
    public:
        TimerTask(int expire, TimeController *controller, std::function<void (TimerTask<INPUT, OUTPUT> *)> && callback): TimerRequest(expire, controller), m_callback(std::move(callback)) { }
	    virtual ~TimerTask()=default;

    public:
        void start()                    { if (!brookOf(this)) { TsingRiver::TsingRiver::startBrookWork(this, nullptr); } }
    public:
        INPUT *getInput()               { return &this->m_input; }
        OUTPUT *getOutput()             { return &this->m_output; }

    protected:
        virtual TrDrip *done()
        {
            TrBrook *brook = brookOf(this);

            if (this->m_callback)
                this->m_callback(this);

            delete this;
            return brook->pop();
        }

    };
}

#endif //__TR_TIMER_TASK__