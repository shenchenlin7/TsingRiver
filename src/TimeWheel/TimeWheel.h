#ifndef __TIME_WHEEL__
#define __TIME_WHEEL__

#include <pthread.h>
#include <stdlib.h>
#include <thread>
#include <chrono>
#include <array>
#include <atomic>
#include <mutex>
#include "list.h"

namespace TimeWheel
{
    #define TIME_WHEEL_SECOND 60
    #define TIME_LEVEL_MINUTE 60
    #define TIME_LEVEL_HOUR   24

    class               SpinLock;
    class               TwQueue;
    class               TwSession;
    class               TimeWheel;

    typedef int64_t     TIME_t;

    class SpinLock
    {
    private:
        std::atomic_flag flag = ATOMIC_FLAG_INIT;
    public:
        SpinLock() = default;
        SpinLock(const SpinLock&) = delete;
        SpinLock& operator= (const SpinLock&) = delete;
    public:
        void lock()     { while(flag.test_and_set(std::memory_order_acquire)) ; }
        void unlock()   { flag.clear(std::memory_order_release); }
    };


    class TwSession
    {
    public:
        int                         m_expire{0};     
        bool                        m_cancel{false};
    public:
        TwSession(int expire): m_expire(expire) { }
        virtual ~TwSession()=default;
    private:
        virtual void timeCallback() = 0;

        friend TimeWheel;
    };

    class TwQueue
    {
    private:
        struct list_head    m_list;
        SpinLock            m_lock;

    public:
        TwQueue()=default;
        virtual ~TwQueue()=default;

    public:
        int init();
        void deinit();

        friend class TimeWheel;
    };

    class TimeWheel
    {
    private:
        struct TwSessionEntry
        {
            struct list_head list;
            TwSession   *session;
        };
    private:
        std::atomic<bool>           m_init{false};
        std::array<TwQueue*, 24>    m_hourWheel;
        std::array<TwQueue*, 60>    m_minuWheel;
        std::array<TwQueue*, 60>    m_secoWheel;
        int                         m_hourIndex{0};
        int                         m_minuIndex{0};
        int                         m_secoIndex{0};
        TIME_t                      m_current{0};
        TIME_t                      m_currentPt;
    public:
        TimeWheel();
        virtual ~TimeWheel();
     public:
        int init();
        void deInit();
        bool addSession(TwSession *session);
    private:
        bool addSessionEntry(TwSessionEntry *entry);
        void timerUpdate();
        void timerExecute();
        void timerShift();
        void moveMinuList(int idx);
        void moveHourList(int idx); 
    public:
        static void *expireTimer(void * arg);  
    };
};

#endif //__TIME_WHEEL__