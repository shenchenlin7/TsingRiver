#include "TimeWheel.h"

namespace TimeWheel
{
   int TwQueue::init()
    {
        INIT_LIST_HEAD(&this->m_list);

        return 0;
    }

    void TwQueue::deinit()
    {
    } 

    TimeWheel::TimeWheel()
    {
        for (auto& it : m_hourWheel)
        {
            it = new TwQueue();
            it->init();
        }

        for (auto& it1 : m_minuWheel)
        {
            it1 = new TwQueue();
            it1->init();
        }

        for (auto& it2 : m_secoWheel)
        {
            it2 = new TwQueue();
            it2->init();
        }
    }

    TimeWheel::~TimeWheel()
    {
        for (auto& it : m_hourWheel)
        {
            it->deinit();
        }

        for (auto& it1 : m_minuWheel)
        {
            it1->deinit();
        }

        for (auto& it2 : m_secoWheel)
        {
            it2->deinit();
        }
    }

    int TimeWheel::init()
    {
        if (true == m_init.load())
            return 1;

        pthread_attr_t attr;
        pthread_t tid;
        if (0 == pthread_attr_init(&attr))
        {
            m_init.store(true);
            bool ret = pthread_create(&tid, &attr, expireTimer, this);
            pthread_attr_destroy(&attr);

            if (0 == ret)
            {
                return 0;
            }

            m_init.store(false);
        }

        return -1;
    }

    void TimeWheel::deInit()
    {
        m_init.store(false);
    }

    bool TimeWheel::addSession(TwSession *session)
    {
        if (session->m_expire <= 0)
            return false;

        struct TwSessionEntry *entry;
        entry = (struct TwSessionEntry*)malloc(sizeof (struct TwSessionEntry));
         if (!entry)
            return false;

        entry->session = session;
        entry->session->m_expire += m_secoIndex;
        bool ret = addSessionEntry(entry);
        if (false == ret)
            free(entry);

        return ret;
    }

    bool TimeWheel::addSessionEntry(TwSessionEntry *entry)
    {
        if (entry->session->m_expire == 0)
        {;  
            std::lock_guard<SpinLock> lock(m_secoWheel[m_secoIndex]->m_lock);
            list_add_tail(&entry->list, &m_secoWheel[m_secoIndex]->m_list);
        }
        else if (entry->session->m_expire < TIME_WHEEL_SECOND)
        {
            int index = entry->session->m_expire;
            std::lock_guard<SpinLock> lock(m_secoWheel[index]->m_lock);
            list_add_tail(&entry->list, &m_secoWheel[index]->m_list);
        } 
        else if (entry->session->m_expire < ((TIME_LEVEL_MINUTE - m_minuIndex)*TIME_WHEEL_SECOND))
        {
            int index = (entry->session->m_expire / TIME_WHEEL_SECOND + m_minuIndex);
            std::lock_guard<SpinLock> lock(m_minuWheel[index]->m_lock); 
            list_add_tail(&entry->list, &m_minuWheel[index]->m_list); 
        } 
        else if (entry->session->m_expire < (TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE*TIME_LEVEL_HOUR))
        {
            entry->session->m_expire += (m_minuIndex*TIME_LEVEL_MINUTE);

            if (entry->session->m_expire < ((TIME_LEVEL_HOUR - m_hourIndex) * TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE))
            {
                int index = (entry->session->m_expire / (TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE) + m_hourIndex);
                std::lock_guard<SpinLock> lock(m_hourWheel[index]->m_lock); 
                list_add_tail(&entry->list, &m_hourWheel[index]->m_list);
            }
            else
            {
                int index = (entry->session->m_expire / (TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE) - (TIME_LEVEL_HOUR - m_hourIndex));
                std::lock_guard<SpinLock> lock(m_hourWheel[index]->m_lock); 
                list_add_tail(&entry->list, &m_hourWheel[index]->m_list);
            }
        }
        
        return true;
    }

    void TimeWheel::timerUpdate() 
    {
        timerExecute();
        timerShift();
        timerExecute();
    }

    void TimeWheel::timerExecute() 
    {
        while (!list_empty(&m_secoWheel[m_secoIndex]->m_list))
        {
            TwSession *session;
            {
                std::lock_guard<SpinLock> lock(m_secoWheel[m_secoIndex]->m_lock); 
                struct TwSessionEntry *entry = list_entry(m_secoWheel[m_secoIndex]->m_list.next, struct TwSessionEntry, list);
                list_del(&entry->list);      
                session = entry->session;
                free(entry);
            }
            session->timeCallback();
        }
	}

    void TimeWheel::timerShift() 
    {
        if ((++m_secoIndex % TIME_WHEEL_SECOND) == 0) 
        {
            m_secoIndex = 0;
            ++m_minuIndex;
            if ((m_minuIndex % TIME_LEVEL_MINUTE) == 0) 
            {
                m_minuIndex = 0;
                ++m_hourIndex;
                if ((m_hourIndex % TIME_LEVEL_HOUR) == 0) 
                {
                    m_hourIndex = 0;	
                }

                std::lock_guard<SpinLock> lock(m_hourWheel[m_hourIndex]->m_lock); 	
                moveHourList(m_hourIndex);
            }
 
            std::lock_guard<SpinLock> lock(m_minuWheel[m_minuIndex]->m_lock); 	  
            moveMinuList(m_minuIndex);
        } 
    }
    
    void TimeWheel::moveMinuList(int idx) 
    {
        while (!list_empty(&m_minuWheel[m_minuIndex]->m_list))
        {
            struct TwSessionEntry *entry = list_entry(m_minuWheel[m_minuIndex]->m_list.next, struct TwSessionEntry, list);
            list_del(&entry->list);
            while (entry->session->m_expire >= TIME_WHEEL_SECOND) 
            { 
                entry->session->m_expire -= TIME_WHEEL_SECOND; 
                if (entry->session->m_expire < 0)
                    entry->session->m_expire = 0;
            }
            
            addSessionEntry(entry);
        }
    }

    void TimeWheel::moveHourList(int idx)
    {
        while (!list_empty(&m_hourWheel[m_hourIndex]->m_list))
        {
            struct TwSessionEntry *entry = list_entry(m_hourWheel[m_hourIndex]->m_list.next, struct TwSessionEntry, list);
            list_del(&entry->list);
            while (entry->session->m_expire >= (TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE)) 
            { 
                entry->session->m_expire -= (TIME_WHEEL_SECOND*TIME_LEVEL_MINUTE); 
                if (entry->session->m_expire < 0)
                    entry->session->m_expire = 0;
            }
            addSessionEntry(entry);
        }
    }

    void *TimeWheel::expireTimer(void * arg)
    {
        TimeWheel *timeWheel = (TimeWheel *)arg;
        std::chrono::steady_clock::time_point start = std::chrono::steady_clock::now();
        while(1)
        {
            if (false == timeWheel->m_init.load())
                break;

            TIME_t diff = std::chrono::duration_cast<std::chrono::milliseconds>(std::chrono::steady_clock::now() - start).count()/1000;
            if (diff > 0) 
            {
                start += std::chrono::seconds(1);
                for (TIME_t i=0 ; i < diff; i++) 
                {
                    timeWheel->timerUpdate();
                }
            }
   
            std::this_thread::sleep_for(std::chrono::milliseconds(200));
        } 

        return NULL;
    }
};