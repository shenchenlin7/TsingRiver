#include "TrFactory.h"

namespace TrTask
{
    ObServerTask *TrFactory::createTaskOb(const std::function<int (ObServerTask *)> subscribe, 
                                               std::function<void (ObServerTask *)> notify)
    {
        return ObWork::createTaskOb(subscribe, notify, nullptr);
    }

    TimeGeneralTask *TrFactory::createTaskTimer(int expire, TimeController *controller, std::function<void (TimeGeneralTask *)> cb)
    {
        return TimerWork::createTaskTimer(expire, controller, cb);
    }

    CommLand::Waterwheel *TrFactory::createWaterwheel()
    {
        CommLand::Waterwheel *waterwheel = new CommLand::Waterwheel();
        if (waterwheel->init() < 0)
        {
            delete waterwheel;
            return nullptr;
        }
        return waterwheel;
    }
}