#include <stdio.h>
#include "TrFactory.h"

int main()
{
    auto createTimerTask = [] (int id, TsingRiver::River *river) {
        TrTask::TimeGeneralTask *timerTask = TrTask::TrFactory::createTaskTimer(id, CommLand::Overman::getWaterwheel(), [] (TrTask::TimeGeneralTask *task) {
            printf("test end");
        });

        (*river)*(timerTask);
    };

    std::thread([createTimerTask](){
        while(1)
        {
            TsingRiver::River *river = TsingRiver::TsingRiver::createRiver([](const TsingRiver::River*){
            });

            createTimerTask(5, river);
            createTimerTask(60, river);
            createTimerTask(360, river);
            createTimerTask(3600, river);
            createTimerTask(7200, river);
            createTimerTask(60000, river);

            river->start();
            std::this_thread::sleep_for(std::chrono::milliseconds(10000));
        }
    }).detach();

    getchar(); // press "Enter" to end.

    return 0;
}
